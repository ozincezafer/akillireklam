package com.example.akillireklam;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class kampanyaDetay extends AppCompatActivity {

    ListView listView;
    PostClass adapter;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference myRef;
    ArrayList<String> firmaAdiFB;
    ArrayList<String> firmaLokasyonEnlemFB;
    ArrayList<String> firmalokasyonBoylamFB;
    ArrayList<String> kampanyaIcerikFB;
    ArrayList<String> kampanyaSuresiFB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kampanya_detay);
        listView =findViewById(R.id.listView);

        firmaAdiFB=new ArrayList<String>();
        firmaLokasyonEnlemFB=new ArrayList<String>();
        firmalokasyonBoylamFB=new ArrayList<String>();
        kampanyaIcerikFB=new ArrayList<String>();
        kampanyaSuresiFB=new ArrayList<String>();

        firebaseDatabase =FirebaseDatabase.getInstance();
        myRef=firebaseDatabase.getReference();

        adapter=new PostClass(firmaAdiFB,firmaLokasyonEnlemFB,firmalokasyonBoylamFB,kampanyaIcerikFB,kampanyaSuresiFB,this);
        listView.setAdapter(adapter);
        getDataFromFirebase();
    }

    public void getDataFromFirebase()
    {
        DatabaseReference newReference=firebaseDatabase.getReference("firmalar");
        newReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot ds:dataSnapshot.getChildren())
                {
                    HashMap<String,String> hashMap=(HashMap<String, String>)ds.getValue();
                    firmaAdiFB.add(hashMap.get("firma_ad"));
                    firmaLokasyonEnlemFB.add(hashMap.get("firma_lokasyon_enlem"));
                    firmalokasyonBoylamFB.add(hashMap.get("firma_lokasyon_boylam"));
                    kampanyaIcerikFB.add(hashMap.get("kampanyali_icerik"));
                    kampanyaSuresiFB.add(hashMap.get("kampanya_suresi"));
                    adapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public  void mapp (View view)
    {
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);

        startActivity(intent);
    }

}
