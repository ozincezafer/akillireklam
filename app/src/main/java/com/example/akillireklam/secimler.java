package com.example.akillireklam;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class secimler extends AppCompatActivity {

    private FirebaseAuth mAuth;
    DatabaseReference mfRef;
    FirebaseDatabase firebaseDatabase;
    // ArrayList<String> firma=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secimler);

    }

    public void sifreDegis(View view) {
        Intent intent = new Intent(getApplicationContext(), sifreDegistir.class);
        startActivity(intent);
    }

    public void kampanyalar(View view) {
        Intent intent = new Intent(getApplicationContext(), kampanyaDetay.class);
        startActivity(intent);
    }
}
