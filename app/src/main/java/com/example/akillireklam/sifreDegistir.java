package com.example.akillireklam;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class sifreDegistir extends AppCompatActivity {

    private FirebaseAuth mAuth;
    EditText yeniSifre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sifre_degistir);
        yeniSifre=(EditText) findViewById(R.id.sifreText);
        mAuth=FirebaseAuth.getInstance();
    }
    public void degistir(View view)
    {
        FirebaseUser user =FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null)
        {
            user.updatePassword(yeniSifre.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(sifreDegistir.this, "Sifre başarı ile değiştirildi!", Toast.LENGTH_SHORT).show();
                            Intent intent =new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(intent);
                        }
                    });
        }
    }
}
