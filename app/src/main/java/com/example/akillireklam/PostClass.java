package com.example.akillireklam;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class PostClass extends ArrayAdapter<String> {
    private final ArrayList<String> firmaAdi;
    private final ArrayList<String> firmaLokasyonEnlem;
    private final ArrayList<String> firmaLokasyonBoylam;
    private final ArrayList<String> kampanyaİcerik;
    private final ArrayList<String> kampanyaSuresi;
    private  final Activity context;

    public PostClass(ArrayList<String> firmaAdi, ArrayList<String> firmaLokasyonEnlem, ArrayList<String> firmaLokasyonBoylam, ArrayList<String> kampanyaİcerik, ArrayList<String> kampanyaSuresi, Activity context) {
        super(context,R.layout.kampanya_view,firmaAdi);
        this.firmaAdi = firmaAdi;
        this.firmaLokasyonEnlem = firmaLokasyonEnlem;
        this.firmaLokasyonBoylam = firmaLokasyonBoylam;
        this.kampanyaİcerik = kampanyaİcerik;
        this.kampanyaSuresi = kampanyaSuresi;
        this.context = context;
    }


    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater =context.getLayoutInflater();
        View firmaView=layoutInflater.inflate(R.layout.kampanya_view,null,true);

        TextView firmaAdiText =firmaView.findViewById(R.id.firmaName);
        TextView firmaLokasyonEnlemText=firmaView.findViewById(R.id.firmaLokasyonLat);
        TextView firmaLokasyonBoylamText=firmaView.findViewById(R.id.firmaLokasyonLot);
        TextView kampanyaİcerikText=firmaView.findViewById(R.id.kampanyaİc);
        TextView kampanyaSuresiText=firmaView.findViewById(R.id.kampanyaTime);

        firmaAdiText.setText(firmaAdi.get(position));
        firmaLokasyonEnlemText.setText(firmaLokasyonEnlem.get(position));
        firmaLokasyonBoylamText.setText(firmaLokasyonBoylam.get(position));
        kampanyaİcerikText.setText(kampanyaİcerik.get(position));
        kampanyaSuresiText.setText(kampanyaSuresi.get(position));




        return firmaView;
    }
}
